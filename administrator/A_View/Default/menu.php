<div id="menu">	
    <div class='user_panel'>
        <div class="pull-left image">
            <img src="<?= $baseurl ?>/a_assets/images/user/1.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>Vagan Sadoyan</p>
            <a href="#"><i class="fa fa-circle"></i> Online</a>
        </div>
    </div>
    <ul class='sidebar-menu'>
        <li class='header'>NAVIGATION</li>
        <li class="menuli _click_next <?= $page == 'contact' || $page == 'partners' || $page == 'partners_chenge' || $page == 'tips' || $page == 'tips_chenge' || $page == 'events' || $page == 'events_chenge' || $page == 'about' || $page == 'partners' || $page == 'partners_chenge' ? "active" : "" ?>">
            <i class="fa fa-book"></i> 
            <span>Main pages</span>
            <span class='open_sub_menu pull-right'><i class="fa fa-chevron-down"></i></span>
        </li>
        <ul class='main_sub_menu <?= $page == 'contact' || $page == 'partners' || $page == 'partners_chenge' || $page == 'tips' || $page == 'tips_chenge' || $page == 'events' || $page == 'events_chenge' || $page == 'about' ? "active_sub" : "" ?>'>
            <li class="header">Pages</li>
            <a href='<?= $baseurl ?>/regions/'>
                <li class='sub_menu <?= $page == 'regions' || $page == 'regions' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span> Regions</span>
                </li>
            </a>
            <a href='<?= $baseurl ?>/homes/'>
                <li class='sub_menu <?= $page == 'homes' || $page == 'homes' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span> Homes</span>
                </li>
            </a>
            <a href='<?= $baseurl ?>/users/'>
                <li class='sub_menu <?= $page == 'alluser' || $page == 'alluser' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span> All Users</span>
                </li>
            </a>

            <a href='<?= $baseurl ?>/gallery/'>
                <li class='sub_menu <?= $page == 'gallery' || $page == 'gallery' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span> Gallery pics</span>
                </li>
            </a>
            <li class="fotter_menu"></li>
        </ul>
    </ul>
</div>
