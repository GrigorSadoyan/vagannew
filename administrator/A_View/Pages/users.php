<div id='content'>

    <div id='table_div'>
        <div class='table_head'>
            <h3></h3>
        </div>
        <div class='table_head'>
            <h3>Users</h3>
        </div>
        <table>
            <thead>
            <tr>
                <th class='table_num'>#</th>
                <th class='table_chek'>
                    <input type="checkbox" class='checkbox_anime' id="all" />
                    <label class='chekbox_label' for="all"></label>
                </th>
                <th class='w_10'>ID #</th>
                <th class='w_10'>Name</th>
                <th class='w_10'>Last Name</th>
                <th class='w_10'>E-Mail</th>
                <th class='w_10'>Phone</th>
                <th class='w_10'>Company</th>
                <th class='w_10'>Date</th>
                <th class='table_action last_th'>Action</th>
            </tr>
            </thead>
            <tbody  data-table=''>
            <?php
            $numbered = 0;

            if (isset($params['a_users'])) {

                foreach ($params['a_users'] as $val) {
                    $numbered++
                    ?>
                    <tr id='m_<?= $val['id'] ?>'>
                        <td>
                            <span><?= $numbered ?></span>
                        </td>
                        <td>
                            <input type="checkbox" class='checkbox_anime sub_chek' id="ch_<?= $val['id'] ?>" data-get='<?= $params['controller'] ?>' data-id="<?= $val['id'] ?>"/>
                            <label class='chekbox_label' for="ch_<?= $val['id'] ?>" ></label>
                        </td>
                         <td>
                            <a href='#'><span><?= $val['id'] ?></span></a>
                        </td>
                        <td>
                            <a href='#'><span><?= $val['name'] ?></span></a>
                        </td>
                        <td>
                            <a href='#'><span><?= $val['lastname'] ?></span></a>
                        </td>
                        <td>
                            <a href='#'><span><?= $val['email'] ?></span></a>
                        </td>
                        <td>
                            <a href='#'><span><?= $val['phone'] ?></span></a>
                        </td>
                        <td>
                            <a href='#'><span><?= $val['company'] ?></span></a>
                        </td>
                        <td>
                            <a href='#'><span><?= $val['data'] ?></span></a>
                        </td>
                        <td class='last_td'>
                            <span class='action_td action_delete' data-id="<?= $val['id'] ?>" data-get='users'><i class="fa fa-trash-o"></i></span>
                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>


    </div>
</div>

