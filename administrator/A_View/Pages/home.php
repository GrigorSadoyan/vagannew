<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">

        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Page Content</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">
                <div class="form_input">
                    <div class="input_group">
                        <select name="tid">

                            <?php foreach ($params['a_regions'] as $val){  ?>
                                <option
                                        <?php if($params['one_home']['tid'] == $val['id']){
                                            echo 'selected';
                                        } ?>
                                        value="<?=$val['id']?>"><?=$val['region']?></option>
                            <?php  } ?>
                        </select>
                    </div>
                </div>
                <div class="form_input">
                    <label>Adress</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='adress' placeholder="Adress" value='<?=$params['one_home']['adress']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Family</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='family' placeholder="Family" value='<?=$params['one_home']['family']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Price</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='price' placeholder="Price" value='<?=$params['one_home']['price']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Description</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='description' placeholder="Description" value='<?=$params['one_home']['description']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Bath</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='bath' placeholder="Bath" value='<?=$params['one_home']['bath']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Bad</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='bad' placeholder="bad" value='<?=$params['one_home']['bad']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Car</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='car' placeholder="car" value='<?=$params['one_home']['car']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Sqeer/Feet</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='sqft' placeholder="Sqeer/Feet" value='<?=$params['one_home']['sqft']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Lat</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='lat' placeholder="Latitude" value='<?=$params['one_home']['lat']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Long</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='long' placeholder="Longitude" value='<?=$params['one_home']['long']?>'>
                    </div>
                </div>

                <div class="form_input">
                    <div class="input_group">
                        <input type="hidden" name="hot_sale" value="off" />
                        Hot Sale
                        <input
                                type="checkbox"
                                class=""
                                name='hot_sale'
                                value="1"
                            <?php if(isset($params['one_home']['hot_sale'])){
                                if($params['one_home']['hot_sale'] == '1'){

                                    echo 'checked';
                                }
                            } ?>
                        >
                    </div>
                </div>

                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>Save</button>
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>

<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>