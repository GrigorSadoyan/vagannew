<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Page Content</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="form_input">
                    <div class="form_input">
                        <label>Upload Galleri image (.jpg,.png)</label>
                        <div class="input_group">
                            <div class='_foto_block foto_block forempty'>
                                <img src='' />
                                <input type="file" name="image" class="img_file" >
                                <div class='empty_foto'><i class="fa fa-picture-o"></i></div>
                                <div class='full_foto'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="real_img" value="<?= isset($params['result'][0]['image']) && $params['result'][0]['image'] != '' ? $params['result'][0]['image'] : ''?>" >
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>




    <div class='box box_a'>
            <?php
            if (isset($params['konktunpics'])) {
                for($i=0;$i<count($params['konktunpics']);$i++){
                    ?>
                    <div class="box_edit box_ck a_galleri">
                        <div class="form_input">
                            <div class="form_input">
                                <label>Galleri images</label>
                                <div class="input_group">
                                    <div class=' foto_block forempty'>
                                        <img src='<?=$baseurlM?>/assets/images/Homes/galleri/<?=$params['konktunpics'][$i]['pic']?>' />
                                    </div>
                                    <div class="a_product_img_del a_product_img_del_icon" data-get="konktunpics" data-id="<?=$params['konktunpics'][$i]['id']?>"><i class="fa fa-ban" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="real_img" value="<?= isset($params['result'][$i]['image']) && $params['result'][$i]['image'] != '' ? $params['result'][$i]['image'] : ''?>" >
                    </div>
                <?php } } ?>
        </div>
    </div>
</div>


<style>
    .a_product_img_del {
        position: absolute;
        display: none;
        top: 100px;
        font-size: 50px;
        cursor: pointer;
        z-index: 100;
    }

    .input_group {
        position: relative;
    }
    .foto_block {
        width: 250px;
        height: 150px;
        border: 1px solid;
        background: #eee;
        cursor: pointer;
        position: relative;
    }
    .input_group:hover .a_product_img_del {
        display: block;
    }
</style>

<script>
    $(document).ready(function() {
        $('.a_product_img_del_icon').click(function(){
            var conf = confirm("Are you sure you want to delete?");
            if(!conf){return false};
            var self=$(this);
            var url = base+"/gallery/add/"+$(this).data('id')+"/delete/";
            var body = "id="+$(this).data('id')+"";
            requestPost(url,body,function(){
                self.parent().parent().fadeOut();
            });
        });
    });
</script>










</div>

<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>