<div id='content'>

        <div id='table_div'>
            <table>
                <thead>
                <tr>
                    <th class='table_num'>#</th>
                    <th class='table_chek'>
                        <input type="checkbox" class='checkbox_anime' id="all" />
                        <label class='chekbox_label' for="all"></label>
                    </th>
                    <th class='w_10'>Name</th>
                    <th class='w_10'>Price $</th>
                    <th class='w_10'>ID #</th>
                    <th class='table_action last_th'>Action</th>
                </tr>
                </thead>
                <tbody  data-table=''>
                <?php
                $numbered = 0;
                if (isset($params['homesall'])) {
                    foreach ($params['homesall'] as $val) {
                        $numbered++
                        ?>
                        <tr id='m_<?= $val['id'] ?>'>
                            <td>
                                <span><?= $numbered ?></span>
                            </td>
                            <td>
                                <input type="checkbox" class='checkbox_anime sub_chek' id="ch_<?= $val['id'] ?>" data-get='homes' data-id="<?= $val['id'] ?>"/>
                                <label class='chekbox_label' for="ch_<?= $val['id'] ?>" ></label>
                            </td>
                            <td>
                               <span><?= $val['adress'] ?></span>
                            </td>
                            <td>
                                <span><?= $val['price'] ?> $</span>
                            </td>
                            <td>
                                <span><?= $val['id'] ?></span>
                            </td>
                            <td class='last_td'>
                                <a href='<?= $baseurl ?>/gallery/add/<?=$val['id']?>/'><span class='action_td'><i class="fa fa-plus" aria-hidden="true"></i></span></a>
                            </td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>