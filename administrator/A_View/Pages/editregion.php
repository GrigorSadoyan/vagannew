<div id='content'>
    <form id='main_form' action='' method='post' enctype="multipart/form-data">
        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Page Content</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box_edit box_ck">
                <div class="form_input">
                    <label> Region Name</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='region' placeholder="Region Name" value="<?=$params['a_regions']['region']?>">
                    </div>
                </div>

                <div class="form_input">
                    <label>Url</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='url' placeholder="Url" value="<?=$params['a_regions']['url']?>">
                    </div>
                </div>

                <div class="form_input">
                    <label>Latitude</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='lat' placeholder="Latitude" value="<?=$params['a_regions']['lat']?>">
                    </div>
                </div>

                <div class="form_input">
                    <label>Longitude</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text" name='long' placeholder="Longitude" value="<?=$params['a_regions']['long']?>">
                    </div>
                </div>
                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>Save</button>
                    </div>
                </div>
            </div>
    </form>
</div>
</div>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>