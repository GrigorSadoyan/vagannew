<?php
    if(isset($params['a_regions'])) {
?>
    <div id='content'>

        <div id='table_div'>
            <div class='table_head'>
                <h3></h3>
            </div>
            <div class='table_head'>
                <h1>Select Region</h1>
            </div>
            <div class='table_head'>
                <div class="form_input">
                    <div class="input_group add_project">
                        <div class="input_img forsave">
                            <i class="fa fa-plus"></i>
                        </div>
                        <a href='<?= $baseurl ?>/homes/add/' class='save'>Add Home</a>
                    </div>
                </div>
            </div>
            <table>
                <thead>
                <tr>
                    <th class='table_num'>#</th>
                    <th class='table_chek'>
                        <input type="checkbox" class='checkbox_anime' id="all"/>
                        <label class='chekbox_label' for="all"></label>
                    </th>
                    <th class='w_10'>Name</th>
                </tr>
                </thead>
                <tbody data-table=''>
                <?php
                $numbered = 0;

                if (isset($params['a_regions'])) {

                    foreach ($params['a_regions'] as $val) {
                        $numbered++
                        ?>
                        <tr id='m_<?= $val['id'] ?>'>
                            <td>
                                <span><?= $numbered ?></span>
                            </td>
                            <td>
                                <input type="checkbox" class='checkbox_anime sub_chek' id="ch_<?= $val['id'] ?>"
                                       data-get='homes' data-id="<?= $val['id'] ?>"/>
                                <label class='chekbox_label' for="ch_<?= $val['id'] ?>"></label>
                            </td>
                            <td>
                                <a href='<?= $baseurl ?>/homes/<?= $val['id'] ?>/'><span><?= $val['region'] ?></span></a>
                            </td>


                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>


        </div>
    </div>
<?php
    }
?>
<!--*****************************************************************************************************************-->
<!--*****************************************************************************************************************-->
<!--*****************************************************************************************************************-->
<!--*****************************************************************************************************************-->

<?php
if(isset($params['a_areg_homes'])){
?>
<?php

    ?>
    <div id='content'>

        <div id='table_div'>
            <div class='table_head'>
                <h3></h3>
            </div>
            <div class='table_head'>
                <h2><?=$params['home_reg']['region']?></h2>
            </div>
            <div class='table_head'>
                <div class="form_input">
                    <div class="input_group add_project">
                        <div class="input_img forsave">
                            <i class="fa fa-plus"></i>
                        </div>
                        <a href='<?= $baseurl ?>/homes/add/' class='save'>Add Home</a>
                    </div>
                </div>
            </div>
            <table>
                <thead>
                <tr>
                    <th class='table_num'>#</th>
                    <th class='table_chek'>
                        <input type="checkbox" class='checkbox_anime' id="all" />
                        <label class='chekbox_label' for="all"></label>
                    </th>
                    <th class='w_10'>Name</th>
                    <th class='w_10'>Price $</th>
                    <th class='w_10'>ID #</th>
                    <th class='table_action last_th'>Action</th>
                </tr>
                </thead>
                <tbody  data-table=''>
                <?php
                $numbered = 0;

                if (isset($params['a_areg_homes'])) {

                    foreach ($params['a_areg_homes'] as $val) {
                        $numbered++
                        ?>
                        <tr id='m_<?= $val['id'] ?>'>
                            <td>
                                <span><?= $numbered ?></span>
                            </td>
                            <td>
                                <input type="checkbox" class='checkbox_anime sub_chek' id="ch_<?= $val['id'] ?>" data-get='homes' data-id="<?= $val['id'] ?>"/>
                                <label class='chekbox_label' for="ch_<?= $val['id'] ?>" ></label>
                            </td>
                            <td>
                                <a href='<?= $baseurl ?>/homes/edit/<?= $val['id'] ?>/'><span><?= $val['adress'] ?></span></a>
                            </td>
                            <td>
                                <a href='<?= $baseurl ?>/homes/edit/<?= $val['id'] ?>/'><span><?= $val['price'] ?> $</span></a>
                            </td>
                            <td>
                                <a href='<?= $baseurl ?>/homes/edit/<?= $val['id'] ?>/'><span><?= $val['id'] ?></span></a>
                            </td>
                            <td class='last_td'>
                                <a href='<?= $baseurl ?>/homes/edit/<?=$val['id']?>/'><span class='action_td'><i class="fa fa-pencil-square-o"></i></span></a>
                                <span class='action_td action_delete' data-id="<?= $val['id'] ?>" data-get='homes'><i class="fa fa-trash-o"></i></span>
                            </td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>


        </div>
    </div>




<?php
    }
?>
