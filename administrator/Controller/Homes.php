<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Region;
use ImageTools\Uploads;
use Model\Homes as home;
use Model\Gallery as ModGals;
use Model\Type;
class Homes extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();

        $requestUri = trim($_SERVER['REQUEST_URI'], '/');
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'homes') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'homes' && $route[1]=='add') {
                $this->addHomePage();
            }elseif ($countRoute == 2 && $route[0] == 'homes' && is_numeric($route[1])) {
                $this->findHomeByReg($route[1]);
            }elseif ($countRoute == 3 && $route[0] == 'homes' && $route[1]=='edit' && is_numeric($route[2])) {
                $this->findHome($route[2]);
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 3 && $route[0] == 'homes' && $route[1]=='edit' && is_numeric($route[2])){
                $this->EditHome($route[2]);
            }elseif ($countRoute == 2 && $route[0] == 'homes' && $route[1]=='add') {
                $this->addHome();
            }elseif ($countRoute == 2 && $route[0] == 'homes' && $route[1]=='delete') {
                $this->DelHome();
            }
        }
    }

    public function index()
    {
        $_oRegions = new Region();
        $_aRegions = $_oRegions->findAll(array());
        $this->result['a_regions']=$_aRegions;
        $this->renderView("Pages/homes","homes",$this->result);
    }

    public function addHomePAge()
    {
        $_oType = new Type();
        $aType = $_oType->findAll(array());
        $this->result['types']=$aType;

        $_oRegions = new Region();
        $_aRegions = $_oRegions->findAll(array());
        $this->result['a_regions']=$_aRegions;
        $this->renderView("Pages/addhome","addhome",$this->result);
    }

    public function addHome()
    {         
        $_oHomes = new home();
        $_aHome=$_POST;
        if(isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            $uploadImg = new Uploads();
            $uploadImg->setImageCurrPath("../assets/images/homes/");
            $uploadImg->setFile($_FILES['image']);
            $curImage = $uploadImg->upload();
            if($curImage['error']){
                  // var_dump($curImage);die;
                $_aProduct['image'] = $curImage['data']['img'];
            }else{
                // echo ('sddfds');die;
                $this->respose(array(
                    'error'=>false,
                    'text'=>$curImage['error_text']
                ));
                die();
            }
        }

        if(isset($_FILES['image']) && $_FILES['image']['name'] == ''){
            $_aProduct['image'] = 1;
        }

        
        $_aHome['pic'] = $_aProduct['image'];
        $_aHome['bath'] = intval($_aHome['bath']);
        $_aHome['bad'] = intval($_aHome['bad']);
        $_aHome['car'] = intval($_aHome['car']);
        $_aHome['hot_sale'] = intval($_aHome['hot_sale']);
        $_aHome['tid'] = intval($_aHome['tid']);
        $_aHome['type_id'] = intval($_aHome['type_id']);
        $_aHome['family'] = intval($_aHome['family']);
        $_oHomes->_post=$_aHome;
        $_aLastId=$_oHomes->insert();
        $tid = $_aHome['tid'];
        header("Location:$this->baseurl/homes/$tid");
    }

    public function findHomeByReg($id)
    {
        $_oHomes = new home();
        $_aHomes = $_oHomes->findByName(array('fild_name'=>'tid','fild_val'=>$id));
        $this->result['a_areg_homes'] = $_aHomes;

        $_oRegions = new Region();
        $_aRegions = $_oRegions->findById($id);
        $this->result['home_reg']=$_aRegions;
        $this->renderView("Pages/homes","homes",$this->result);
    }

    public function findHome($id)
    {
        $_oHome = new home();
        $_aHome = $_oHome->findById($id);
        $this->result['one_home']=$_aHome;
        $_oRegions = new Region();
        $_aRegions = $_oRegions->findAll(array());
        $this->result['a_regions']=$_aRegions;
        $this->renderView("Pages/home","home",$this->result);
    }

    public function EditHome($id)
    {
        $_oHome = new home();
        $_oHome->setId($id);
        $_oHome->_put=$_POST;
        $_oHome->update();
        $_aHome = $_oHome->findById($id);
        $tid = $_aHome['tid'];
        header("Location:$this->baseurl/homes/$tid");
    }

    public function DelHome()
    {
        $MyId = $_POST['id'];
        $_oGalleri = new ModGals();
        $_MyPics = $_oGalleri->findByName(array('fild_name'=>'tunid','fild_val'=>$MyId));
        for ($i=0; $i<count($_MyPics); $i++){
            unlink("../assets/images/homes/galleri/".$_MyPics[$i]['pic']);
        }
        $_oGalleri->delFildName = 'tunid';
        $_oGalleri->delValue = $MyId;
        $_oGalleri->delete();
        $_oHome = new home();
        $_aHome = $_oHome->findById($MyId);
        unlink("../assets/images/homes/".$_aHome["pic"]);

        $_oHome->delFildName = 'id';
        $_oHome->delValue = $_POST['id'];
        $_oHome->delete();
        header("Location:$this->baseurl/homes/");

    }
}
