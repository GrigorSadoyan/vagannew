<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\User;

class Users extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();

        $requestUri = trim($_SERVER['REQUEST_URI'], '/');
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            if($countRoute == 1 && $route[0]=='users'){
                $this->index();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'users' && $route[1]=='delete'){
                $this->DeleteUSer();
            }
        }
    }

    public function index()
    {
        $_oUsers= new User();
        $_aUsers = $_oUsers->findAll(array('order'=>array('desc'=>'id')));
        $this->result['a_users']=$_aUsers;
        $this->renderView("Pages/users","users",$this->result);
    }

    public function DeleteUSer()
    {
        $_oUsers= new User();
        $_oUsers->delFildName = 'id';
        $_oUsers->delValue = $_POST['id'];
        $_oUsers->delete();
        header("Location:$this->baseurl/homes/");
    }
}
