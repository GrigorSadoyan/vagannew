<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Homes;
use Model\Gallery as ModGals;
class Gallery extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();

        $requestUri = trim($_SERVER['REQUEST_URI'], '/');
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            if($countRoute == 1 && $route[0]=='gallery'){
                $this->index();
            }elseif($countRoute == 3 && $route[0]=='gallery'  && $route[1]=='add' && is_numeric($route[2])){
                $this->addPage($route[2]);
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 3 && $route[0]=='gallery'  && $route[1]=='add' && is_numeric($route[2])){
                $this->addGalleryPics($route[2]);
            }elseif($countRoute == 4 && $route[0]=='gallery'  && $route[1]=='add' && is_numeric($route[2]) && $route[3] == 'delete'){
                $this->delTunPics($route[2]);
            }
        }

    }

    public function index()
    {
        $_oHomes = new Homes;
        $_aHomes = $_oHomes->findAll(array());
        $this->result['homesall']=$_aHomes;
        $this->renderView("Pages/gallery","gallery",$this->result);
    }

   public function addPage($id)
    {
        $_oGalleri = new ModGals();
        $_aGalleri = $_oGalleri->findByName(array('fild_name'=>'tunid','fild_val'=>$id,'order'=>array('desc'=>'id')));
        $this->result['konktunpics']=$_aGalleri;
        $this->renderView("Pages/addgalpic","addgalpic",$this->result);
    }

    public function addGalleryPics($id)
    {
        // var_dump($_POST);
        // var_dump($_FILES);die;
        // $_aProduct = $_POST;
        $_oGalleri = new ModGals();
        if(isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            $uploadImg = new \ImageTools\Uploads();
            $uploadImg->setImageCurrPath("../assets/images/Homes/galleri/");
            $uploadImg->setFile($_FILES['image']);
            $curImage = $uploadImg->upload();
            if($curImage['error']){
                $_aProduct['image'] = $curImage['data']['img'];
            }else{
                $this->respose(array(
                    'error'=>false,
                    'text'=>$curImage['error_text']
                ));
                die();
            }
        }
        $_oGalleri->_post=array(
            'pic'=> $_aProduct['image'],
            'tunid'=> $id
        );
        $this->lastId = $_oGalleri->insert();
        header("Location:$this->baseurl/gallery/add/$id");
    }
    public function delTunPics($id)
    {
        $_oGalleri = new ModGals();
        $_oGalleri->delFildName = 'id';
        $_oGalleri->delValue = $_POST['id'];
        $_oGalleri->delete();
        header("Location:$this->baseurl/gallery/add/$id");
    }
}
