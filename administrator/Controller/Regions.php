<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Region;

class Regions extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();

        $requestUri = trim($_SERVER['REQUEST_URI'], '/');
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            if($countRoute == 1 && $route[0]=='regions'){
                $this->index();
            }elseif($countRoute == 2 && $route[0]=='regions' && $route[1]=='addregion'){
                $this->AddRegionPage();
            }elseif($countRoute == 3 && $route[0]=='regions' && $route[1]=='edit' && is_numeric($route[2])){
                $this->editRegionPage($route[2]);
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0]=='regions' && $route[1] == 'addregion'){
                $this->AddRegion();
            }elseif($countRoute == 2 && $route[0]=='regions' && $route[1]=='delete'){
                $this->RegionNamedel();
            }elseif($countRoute == 3 && $route[0]=='regions' && $route[1]=='edit' && is_numeric($route[2])){
                $this->editRegion($route[2]);
            }
        }

    }

    public function index()
    {
        $_oRegions = new Region();
        $_aRegions = $_oRegions->findAll(array());
        $this->result['a_regions']=$_aRegions;
        $this->renderView("Pages/regions","regions",$this->result);
    }

    public function AddRegionPage()
    {
        $this->renderView("Pages/addregion","addregion",$this->result);
    }

    public function AddRegion()
    {
        $_oRegions = new Region();
        $_aRegions=$_POST;
        $_oRegions->_post=$_aRegions;
        $_aLastId=$_oRegions->insert();
        header("Location:$this->baseurl/regions/");
    }

    public  function RegionNamedel()
    {
        $_oRegions = new Region();
        $_oRegions->delFildName = 'id';
        $_oRegions->delValue = $_POST['id'];
        $_oRegions->delete();

        header("Location:$this->baseurl/regions/");
    }

    public function editRegionPage($id)
    {
        $_oRegions = new Region();
        $_aRegions = $_oRegions->findById($id);
//        var_dump($_aRegions);die;
        $this->result['a_regions']=$_aRegions;
        $this->renderView("Pages/editregion","editregion",$this->result);
    }
    public function editRegion($id)
    {
        $_oRegions = new Region();
        $_oRegions->setId($id);
        $_oRegions->_put=$_POST;
        $_oRegions->update();
        header("Location:$this->baseurl/regions/");

    }
}
