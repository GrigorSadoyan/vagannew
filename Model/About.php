<?php
/**
 * Created by PhpStorm.
 * Myhouses: Grigor
 * Date: 4/15/2017
 * Time: 2:31 PM
 */

namespace Model;
use Core\Model;


class About extends Model
{
    protected $table='about';
    public function __construct()
    {
        parent::__construct();
    }
}