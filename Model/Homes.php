<?php
/**
 * Created by PhpStorm.
 * Myhouses: Grigor
 * Date: 4/15/2017
 * Time: 2:31 PM
 */

namespace Model;
use Core\Model;
use \PDO;

class Homes extends Model
{
    protected $table='homes';
    public function __construct()
    {
        parent::__construct();
    }

    public function findResents(){
        $this->pdoObject = $this->db->prepare("SELECT * FROM `$this->table` ORDER BY `data` DESC LIMIT 6 ");
        $this->pdoObject->execute();
        return $this->pdoObject->fetchAll(PDO::FETCH_ASSOC);
    }
    public function SelectFilter($region, $minprice, $maxprice){
        $this->pdoObject = $this->db->prepare("SELECT * FROM `$this->table` WHERE `tid` = :regid AND `price` BETWEEN :minprice AND :maxprice ORDER BY `id` DESC");
        $this->pdoObject->bindValue(':regid',$region,PDO::PARAM_INT);
        $this->pdoObject->bindValue(':minprice',$minprice,PDO::PARAM_INT);
        $this->pdoObject->bindValue(':maxprice',$maxprice,PDO::PARAM_INT);
        $this->pdoObject->execute();
        return $this->pdoObject->fetchAll(PDO::FETCH_ASSOC);
    }
}