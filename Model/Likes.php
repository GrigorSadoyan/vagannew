<?php
/**
 * Created by PhpStorm.
 * Myhouses: Grigor
 * Date: 4/15/2017
 * Time: 2:31 PM
 */

namespace Model;
use Core\Model;
use PDO;

class Likes extends Model
{
    protected $table='likes';
    public function __construct()
    {
        parent::__construct();
    }

    public function deleteBag($code)
    {
        $cod = intval($code);
        if(isset($_COOKIE['user_in'])){
            $tok = $_COOKIE['user_in'];
        }

        $this->pdoObject = $this->db->prepare("DELETE FROM `$this->table` WHERE `tid`=:valf AND `token` =:vals");

        $this->pdoObject->bindValue(':valf',$cod,PDO::PARAM_INT);
        $this->pdoObject->bindValue(':vals',$tok,PDO::PARAM_STR);
        $this->pdoObject->execute();
        return $this->pdoObject->fetchAll(PDO::FETCH_ASSOC);
    }
}