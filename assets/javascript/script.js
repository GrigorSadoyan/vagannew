$(document).ready(function(){
    var HeadHeight = $('header').height();
    var Headwidth = $('header').width();
    //console.log(Headwidth);
    if(Headwidth >= 768){
        var ContentHeight = $(window).height();
        var MyHeight = ContentHeight - HeadHeight-5;
        $('#map').height(MyHeight);
        $('.my_map').height(MyHeight);
    }
    $('.single-item').slick({
        autoplay: true,
        dots: false,
        speed:500,
        autoplaySpeed: 2000,
        prevArrow:false,
        nextArrow:false,
        fade: true
    });
    $('.single-items').slick({
        autoplay: true,
        speed:500,
        autoplaySpeed: 2000,
        dots: true,
        fade: true,
        prevArrow:"<div class='a_left_home control-c prev slick-prev'><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></div>",
        nextArrow:"<div class='a_right_home control-c next slick-next'><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></div>"
    });
    $('.name_cl').click(function () {
        $('.change_pass').slideDown();
    })
    $('.old_pass_clean').keyup(function () {
        var myCook = getCookie('user_in');
        var myEmail = $('.old_pass_clean_my').val();
        var MyPassword = $(this).val();
        var hash = CryptoJS.SHA1(myEmail+MyPassword);
        var resultPassword = CryptoJS.enc.Hex.stringify(hash);
        if(myCook == resultPassword){
            $(this).css({
                'box-shadow': '0px 0px 2px #0abe51'
            });
        }else{
            $(this).css({
                'box-shadow': '0px 0px 2px red'
            });
        }
    });
    $('.change_renew').keyup(function () {
        var MyPassword = $(this).val();
        var MyRePassword = $('.change_new').val();
        if(MyPassword == MyRePassword){
            $(this).css({
                'box-shadow': '0px 0px 2px #0abe51'
            });
            $('.change_new').css({
                'box-shadow': '0px 0px 2px #0abe51'
            });
            if($('.change_new').val() == ''){
                $('.change_new').css({
                    'box-shadow': '0px 0px 2px white'
                });
            }
            if($(this).val() == ''){
                $(this).css({
                    'box-shadow': '0px 0px 2px white'
                });
            }
        }else{
            $(this).css({
                'box-shadow': '0px 0px 2px red'
            });
            $('.change_new').css({
                'box-shadow': '0px 0px 2px red'
            });
        }
    });
    $('.change_new').keyup(function () {
        var MyPassword = $(this).val();
        var MyRePassword = $('.change_renew').val();
        if(MyPassword == MyRePassword){
            $(this).css({
                'box-shadow': '0px 0px 2px #0abe51'
            });
            $('.change_renew').css({
                'box-shadow': '0px 0px 2px #0abe51'
            });
            if($('.change_renew').val() == ''){
                $('.change_renew').css({
                    'box-shadow': '0px 0px 2px white'
                });
            }
            if($(this).val() == ''){
                $(this).css({
                    'box-shadow': '0px 0px 2px white'
                });
            }
        }else{
            $(this).css({
                'box-shadow': '0px 0px 2px red'
            });
            $('.change_renew').css({
                'box-shadow': '0px 0px 2px red'
            });
        }
    })
    // $('.search_homes').click(function () {
    //     var myinputs = $('.filter_items').find('input , select');
    //     var region =$(myinputs[0]).val();
    //     var minprice =$(myinputs[1]).val();
    //     var maxprice =$(myinputs[2]).val();
    //     if(minprice == ''){
    //         minprice = '0';
    //     }
    //     if(maxprice == ''){
    //         maxprice = '10000000';
    //     }
    //     var url = base+"/region/headsearch/";
    //     var body = "region="+region+"&minprice="+minprice+"&maxprice="+maxprice+"";
    //     requestPost(url,body,function(){
    //
    //     })
    // })
    
    $('.__but_like').click(function () {
        if($(this).hasClass('like_active')) {
            var self = $(this);
            var id = $(this).attr('data-code');

            var url = base + "/region/home/like/delete/";
            var body = "id=" + id + "";
            requestPost(url,body,function(result){
                self.removeClass('like_active');
            });
        }else{
            var self = $(this);
            var id = $(this).attr('data-code');

            var url = base + "/region/home/like/add/";
            var body = "id=" + id + "";
            requestPost(url, body, function () {
                    self.addClass('like_active');
            });
            return false;
       }
    });
    $('.__del_fav').click(function () {
        var self = $(this);
        var id = $(this).attr('data-code');
        var url = base + "/region/home/like/delete/";
        var body = "id=" + id + "";
        requestPost(url,body,function(result){
            self.parent('div').fadeOut();
        });
        return false;
    });
    $("a.grouped_elements").attr("rel", "group1").fancybox();
});




function setCookie(cname, exdays,callback) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname+";" + expires + ";path=/";
    callback();
}
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function deleteCookie(name) {
    document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
}

function requestPost(url,body,callback){
    var oAjaxReq = new XMLHttpRequest();
    oAjaxReq.open("post", url, true);
    oAjaxReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    oAjaxReq.send(body);
    oAjaxReq.onreadystatechange = callback;
}

$(document).ready(function () {
    $('.search_mobail_btns').click(function () {
        $('.serch_opacity').toggle();
        $('.main_search').toggle();
    })
    $(".main_search").click(function(e) {
        e.stopPropagation();
    });
})
$('.my_map_mod').children('div').css({
    'z-index':'10000'
})