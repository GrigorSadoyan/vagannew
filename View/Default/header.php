<!doctype html>
<html lang="en">
<head>
    <title>dbigllc.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="<?= $baseurl ?>/assets/images/content/Equal_Housing_Opportunity_black.png" />
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/slick/slick-theme.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/javascript/lib/jquery.fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" file="text/css" href="<?= $baseurl ?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/style.css" media="all" />
    <script type="text/javascript"><?php echo "var base = '".$baseurl."';"; ?></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/sha1.js"></script>
    <script src="<?= $baseurl ?>/assets/javascript/jquery2.2.4.min.js"></script>
    <script src="<?= $baseurl ?>/assets/css/bootstrap/js/bootstrap.js"></script>
    <script src="<?= $baseurl ?>/assets/css/slick/slick.js"></script>
    <script type="text/javascript" src="<?= $baseurl ?>/assets/javascript/lib/jquery.fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="<?= $baseurl ?>/assets/javascript/script.js"></script>
</head>
<body>
<header class="greenColor">
    <div class="content clear" >
        <div class="col-md-12  nopadd clear mainheader" >
            <div class="col-md-4 col-lg-4 col-sm-4 left">
                <div class="head_logo">
                    <a href="<?=$baseurl?>">
                        <img src="<?=$baseurl?>/assets/images/content/dlsLogo.png" width="100%">
                    </a>
                </div>
            </div>
            <div class="col-md-8 col-lg-5 col-sm-8 col-xs-12 right mob_right">
                <?php   if(!isset($_COOKIE['user_in'])){ ?>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-3 nopadd menu_items mobailLogo" >
                        <a href="<?=$baseurl?>/assets/pdf/updatedlist.pdf" target="_blank"> <span></span></a>
                    </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-3 nopadd menu_items" >
                    <a href="<?=$baseurl?>/assets/pdf/updatedlist.pdf" target="_blank"> <span>List</span></a>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-3 nopadd menu_items">
                    <span data-toggle="modal" data-target="#myModal">Log In</span>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-3 nopadd menu_items" >
                    <span data-toggle="modal" data-target="#myModal1">Sign Up</span>
                </div>
                <?php   } ?>
                <?php   if(isset($_COOKIE['user_in'])){ ?>
                <?php   if(strlen($_COOKIE['user_in']) == 40){ ?>
                <div class="col-md-4 col-lg-4 col-sm-4 nopadd menu_items">
                    <span class="name_cl"><?=$this->userInfo['name']?></span>
                    <div class="change_pass"><a href="<?= $baseurl ?>/login/changepassword/">Change Password</a></div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 nopadd menu_items">
                    <a href="<?= $baseurl ?>/region/favourite/"><span>Favourite</span></a>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 nopadd menu_items">
                    <a href="<?= $baseurl ?>/login/logout/"><span>Log Out</span></a>
                </div>
                <?php   } } ?>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="login_main">
                <form method="post" action="<?=$baseurl?>/login/log/">
                    <div class="sign_title">
                        <p>LOG IN</p>
                        <p class="log_in_text">Please type your E-Mail and Password for Log In</p>
                    </div>
                    <div class="sign_row">
                        <input type="email" name="email" placeholder="E-Mail" required>
                    </div>
                    <div class="sign_row">
                        <input type="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="sign_row">
                        <input class="sign_butt" type="submit" value="SUBMIT">
                    </div>
                    <p class="forgot_text"><a href="<?= $baseurl ?>/login/forgotpassword/"><span>Forgot password?</span></a></p>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
            <div class="login_main">
                <form method="post" action="<?=$baseurl?>/login/reg/">
                    <div class="sign_title">
                        <p>LOG IN</p>
                        <p class="log_in_text">Please type your personal information for Registration</p>
                    </div>
                    <div class="sign_row">
                        <input type="text" name="name" placeholder="Name" required>
                    </div>
                    <div class="sign_row">
                        <input type="text" name="lastname" placeholder="Lastname" required>
                    </div>
                    <div class="sign_row">
                        <input type="text" name="phone" placeholder="Phone Number" required>
                    </div>
                    <div class="sign_row">
                        <input type="email" name="email" placeholder="E-Mail" required>
                    </div>
                    <div class="sign_row">
                        <input type="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="sign_row">
                        <p class="log_in_text">By clicking on the SUBMIT button you agree width <br/><a href="<?=$baseurl?>/login/terms/"><u class="terming">terms of use</u></a></p>
                        <input class="sign_butt" type="submit" value="SUBMIT">
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>

<div class=" search_mobail_btns">
    <i class="fa fa-search fa-2x" aria-hidden="true"></i>
</div>
