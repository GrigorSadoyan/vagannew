<div class="col-md-12 footer_main">
    <div class="content clear">
        <div class="cont_inf_png clear">
            <div class="col-md-4 col-sm-4 m_from_top">
                <div class="col-md-6 col-sm-12">
                    <p class="foot_text_title">Company</p>
                    <p class="foot_text_items">About</p>
                    <p class="foot_text_items">Blog</p>
                    <a href="<?=$baseurl?>/sell/"><p class="foot_text_items">Sell Your Home</p></a>
                    <a href="<?=$baseurl?>/terms/"><p class="foot_text_items">Terms & Privacy</p></a>
                </div>
                <div class="col-md-6 mob_hidd">
                    <p class="foot_text_title">Discover</p>
                    <a href="<?=$baseurl?>/assets/pdf/updatedlist.pdf" target="_blank"><p class="foot_text_items">Properties List</p></a>
                    <p class="foot_text_items"  data-toggle="modal" data-target="#myModal" style="cursor:pointer;">Sign In</p>
                    <p class="foot_text_items">Careers</p>
                    <p class="foot_text_items">Widgets</p>
                </div>
                <div class="col-md-12 col-sm-12 nopadd clear">
                    <div class="col-md-2 col-sm-3 my_socials">
                        <a href="https://www.instagram.com/dlshomesonline/" target="_blank"> <span class="social_icons"><i class="fa fa-instagram" aria-hidden="true"></i></span></a>
                    </div>
                    <div class="col-md-2 col-sm-3 my_socials">
                        <a href="https://www.facebook.com/dlshomesonline/" target="_blank"> <span class="social_icons"><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                    </div>
                    <div class="col-md-2 col-sm-3 my_socials">
                        <span class="social_icons"><i class="fa fa-linkedin" aria-hidden="true"></i></span>
                    </div>
                    <div class="col-md-2 col-sm-3 my_socials">
                        <span class="social_icons"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 nopadd">
                <div class="col-md-12 col-sm-12 m_from_top">
                    <div class="col-md-6 col-sm-12 clear ">
                        <div class="col-md-12 mob_hidd">
                            <div class="foot_eq_host_image">
                                <img src="<?=$baseurl?>/assets/images/content/Equal_Housing_Opportunity.png" alt="Equal_Housing_Opportunity" width="70%">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="cont_inf">
                                <span class="cont_inf_text">Saab Realty Inc</span>
                            </div>
                            <div class="cont_inf">
                                <span class="cont_inf_text">Elya Damiryan</span>
                            </div>
                            <div class="cont_inf">
                                <span class="cont_inf_text">Licensed R.E. Salesperson</span>
                            </div>
                            <div class="cont_inf">
                                <span class="cont_inf_text"><a class="tel_link" href="tel:917-416-3149">(917)416-3149 Phone</a></span>
                            </div>
                            <div class="cont_inf">
                            <span class="cont_inf_text">
                                 <a class="tel_link" href="mailto:dlshomes@outlook.com" target="_top">
                                     dlshomes@outlook.com
                                 </a>
                            </span>
                            </div>
                            <div class="cont_inf">
                                <span class="cont_inf_text">718-799-1417 Fax</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 clear col-sm-12 ">
                        <div class="cont_agent_box">
                            <form action="" method="post">
                                <input class="b_inp" type="text" name="name" placeholder="Name">
                                <input class="b_inp" type="text" name="phone" placeholder="Phone">
                                <input class="b_inp" type="email" name="email" placeholder="Your E-mail adress">
                                <textarea name="text" class="b_inp" placeholder="Message"></textarea>
                                <input class="b_inp b_inp_submit" type="submit" name="submit" value="Send">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="рequest_call" id="req_calling">
    <a class="tel_link" href="tel:917-416-3149"><p><i class="fa fa-phone" aria-hidden="true"></i></p></a>
</div>
<div class="col-xs-12 mobail_footer">
    <div class="col-xs-3 icon_mob_icon">
        <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i></a>
    </div>
    <div class="col-xs-3 icon_mob_icon">
        <a href=""><i class="fa fa-heart-o" aria-hidden="true"></i></a>
    </div>
    <div class="col-xs-3 icon_mob_icon">
        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i></a>
    </div>
    <div class="col-xs-3 icon_mob_icon">
        <a href="tel:917-416-3149"><i class="fa fa-phone" aria-hidden="true"></i></a>
    </div>
</div>
</body>
</html>
