<div class="col-md-12 sellyourhome_main">
    <div class="col-md-8 center sell_home">
        <div class="col-md-12 ">
            <div class="col-md-2">
                <img src="http://www.dlshomesonline.com/assets/images/DesignImages/Equal_Housing_Opportunity_black.png" alt="Equal_Housing_Opportunity" width="100%">
            </div>
        </div>
        <div class="col-md-12 sell_home_text">
            <span>My name is Elya Damiryan from Saab Realty Inc.</span>
        </div>
        <div class="col-md-9 center sell_home_text_f sxt_f">
            <span><b>HERE ARE THE REASONS TO HIRE ME TO SELL YOUR HOME:</b></span>
        </div>
        <div class="col-md-12">
            <div class="col-md-11 center sell_home_text">
                <span>
                    The sale of your home is one of the biggest financial transactions of your life and I am
                    sure that you want it to go as smoothly as possible. Here are some examples of why I can
                    accomplish this for you:
                </span>
            </div>
        </div>
        <div class="col-md-12 sell_home_text">
            <div class="col-md-10 center sell_home_text">
                <ol>
                    <li>
                        <div class=" sell_home_text_li">
                            <span>
                                I’ve sold 100’s of homes and got my clients an excellent price. I will help you set
                                the optimal price for your home. I will prepare and provide a competitive market
                                analysis for your property and explain it to you in complete detail.
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class=" sell_home_text_li">
                            <span>
                                I will advertise for you. Through my websites and various real estate programs, I
                                will be able to reach a wide audience and be able to find you the best buyer. I will
                                schedule showings around your schedule and have open house events to get the
                                job done quickly.
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class=" sell_home_text_li">
                            <span>
                                Save yourself time. I will do the work so that you don’t have to. I’ll
                                communicate with all potential buyers on your behalf and bargain to get you the
                                best price.
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class=" sell_home_text_li">
                            <span>
                                I will help you in making your home look its best. This is vital to attracting
                                buyers. I’ll determine what home buyers want and make
                                sure that your home makes the best impression.
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class=" sell_home_text_li">
                            <span>
                                I have access to a multiple listing service. This will accomplish giving your home
                                the exposure that it needs and helps you find motivated buyers.
                            </span>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-11 center s_home_t">
                <span>
                    If you are serious about selling your home, call me to set up an appointment
                    for us to meet and I will provide you with a complimentary Competitive
                    Market Analysis.
                </span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-11 center s_home_t_s">
                <span>
                    Hope to hear from you.<br>
                    <u>Elya Damiryan</u><br>
                    <u><a class="tel_link" href="tel:917-416-3149">917-416- 3149</a></u>
                </span>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>