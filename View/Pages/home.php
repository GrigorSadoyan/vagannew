<?php

?>
<div class="col-md-12 col-sm-12 nopadd clear">
    <div class="col-md-6 col-sm-6 nopadd my_map" id="map">
    </div>
    <div class="col-md-6 col-sm-6 nopadd ">
        <div class="col-md-12 col-sm-12 nopadd my_information my_map">
            <div class=" single-items home_slide_pics">
                <div class="h_slide_img">
                    <?php   if($params['this_home']['pic'] == 1 ){  ?>
                        <img src="<?=$baseurl?>/assets/images/homes/Coming-Soon-Sign.jpg" alt="Coming-Soon-Sign" width="100%">
                    <?php  }else {  ?>
                        <img src="<?=$baseurl?>/assets/images/homes/<?=$params['this_home']['pic']?>" alt="" width="100%">
                    <?php   }  ?>


                </div>
                <?php if(count($params['gallery']) != 0){ ?>
                        <?php foreach ($params['gallery'] as $gals){ ?>
                            <div class="h_slide_img">
                                    <img src="<?=$baseurl?>/assets/images/homes/galleri/<?=$gals['pic']?>" width="100%">
                            </div>
                    <?php   }  ?>
                <?php   }  ?>
            </div>
            <div class="col-md-12 clear nopadd my_homes_desc">
                <div class="col-md-12 nopadd clear">
                    <div class="col-md-11 nopadd clear">
                        <p class="my_home_name"><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$params['this_home']['adress']?></p>
                        <p class="my_home_id">ID# <?=$params['this_home']['id']?></p>
                    </div>
                    <?php  if(isset($_COOKIE['user_in'])){ ?>
                        <?php  if(strlen($_COOKIE['user_in']) == 40){ ?>
                        <div class="col-md-1 nopadd clear like_home_block">
                            <span
                                    data-code="<?=$params['this_home']['id']?>"
                                    class="__but_like <?php if(count($params['this_home']['likes']) != 0){ echo 'like_active'; } ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </span>
                        </div>
                    <?php }  } ?>
                </div>
                <div class="col-md-12 nopadd clear">
                    <div class="col-md-2 clear">
                        <p class="my_home_bed"><i class="fa fa-bed" aria-hidden="true"></i> <span><?=$params['this_home']['bad']?></span></p>
                    </div>
                    <div class="col-md-2">
                        <p class="my_home_bed"><i class="fa fa-bath" aria-hidden="true"></i> <span><?=$params['this_home']['bath']?></span></p>
                    </div>
                    <?php
                        if($params['this_home']['car'] != '0') {
                            ?>
                            <div class="col-md-2">
                                <p class="my_home_bed">
                                    <i class="fa fa-car" aria-hidden="true"></i>
                                    <span><?= $params['this_home']['car'] ?></span>
                                </p>
                            </div>
                            <?php
                        }else{
                    ?>
                        <p></p>
                    <?php
                        }
                    ?>
                    <div class="col-md-6">
                        <p class="my_home_bed"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <span><?=$params['this_home']['sqft']?> sq/ft</span></p>
                    </div>
                </div>
                <div class="col-md-12 nopadd clear">
                    <div class="col-md-3 clear">
                        <p class="my_home_bed" style="margin-left: 2%;">
                            Fam.
                            <span><?=$params['this_home']['family']?></span>
                        </p>
                    </div>

                    <div class="col-md-8 clear">
                        <p class="my_home_bed" style="margin-left: 18%;">
                            Price
                            <span>
                                <i class="fa fa-usd" aria-hidden="true"></i><span class="my_price_mains" style="padding-left: 0px;"><?=$params['this_home']['price']?></span>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="col-md-12 clear nopadd ">
                    <div class="col-md-12 clear">
                        <p class="my_home_desc">Description</p>
                        <p class="my_home_descr">
                            <?=$params['this_home']['description']?>
                        </p>
                    </div>
                </div>
            </div>
<!--            <div class="col-md-12 clear my_similars">-->
<!--                <p class="similar_title">Similar Listings</p>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="my_similar">-->
<!--                        <div class="similar_img">-->
<!--                            <img src="--><?//=$baseurl?><!--/assets/images/homes/1.png" alt="address">-->
<!--                        </div>-->
<!--                        <div class="similar_address">-->
<!--                            <p>39 Remsen St, Brooklyn, NY 11201, USA</p>-->
<!--                        </div>-->
<!--                        <div class="similar_price">-->
<!--                            <p>$ 899,000.00</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="my_similar">-->
<!--                        <div class="similar_img">-->
<!--                            <img src="--><?//=$baseurl?><!--/assets/images/homes/1.png" alt="address">-->
<!--                        </div>-->
<!--                        <div class="similar_address">-->
<!--                            <p>39 Remsen St, Brooklyn, NY 11201, USA</p>-->
<!--                        </div>-->
<!--                        <div class="similar_price">-->
<!--                            <p>$ 899,000.00</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="my_similar">-->
<!--                        <div class="similar_img">-->
<!--                            <img src="--><?//=$baseurl?><!--/assets/images/homes/1.png" alt="address">-->
<!--                        </div>-->
<!--                        <div class="similar_address">-->
<!--                            <p>39 Remsen St, Brooklyn, NY 11201, USA</p>-->
<!--                        </div>-->
<!--                        <div class="similar_price">-->
<!--                            <p>$ 899,000.00</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</div>
<script>
    var ThisHome  = <?= json_encode($params['this_home']); ?>;
    var regions = <?= json_encode($params['reg_name']); ?>;

    var obj = <?= json_encode($params['homes']); ?>;
    var y;
    var Region = [regions.lat,regions.long,regions.id];
    var locations = [];
    for(y = 0; y <obj.length; y++ ){
        var a =[obj[y].adress, obj[y].lat, obj[y].long,obj[y].pic,obj[y].id];
        locations.push(a);
    }
    //console.log(ThisHome);
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: new google.maps.LatLng(Region[0], Region[1]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow();

        var marker, i,assets;
        var icon = {
            icon:base + "/assets/images/content/home_marker.png"
        };
        var RedIcon = {
            RedIcon:base + "/assets/images/content/marker_red.png"
        };
        for (i = 0; i < locations.length; i++) {
            if(locations[i][1] == ThisHome.lat && locations[i][2] == ThisHome.long){
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    icon: RedIcon.RedIcon,
                    map: map
                });
            }else{
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    icon: icon.icon,
                    map: map
                });
            }
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    if(locations[i][3] == 1){
                        infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+regions.id+'/'+locations[i][4]+'/"><img src="'+base+'/assets/images/homes/Coming-Soon-Sign.jpg" width="100%"></a></div><div class="map_home_name"><span>'+locations[i][0]+'</span></div>');
                    }else{
                        infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+regions.id+'/'+locations[i][4]+'/"><img src="'+base+'/assets/images/homes/'+locations[i][3]+'" width="100%"></a></div><div class="map_home_name"><span>'+locations[i][0]+'</span></div>');
                    }
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6kwbUaYPPDJltTBXnLwEwVgvrklMBKkg&callback=initMap">
</script>