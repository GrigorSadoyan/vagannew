<div class="clear">
    <div class=" col-md-12 main_had  clear nopadd">
            <div id="map" class="my_map_main">
            </div>
        <script>
            var obj = <?= json_encode($params['homeall']); ?>;
            var y;
            var locations = [];
            for(y = 0; y <obj.length; y++ ){
                var a =[obj[y].adress, obj[y].lat, obj[y].long,obj[y].pic,obj[y].id, obj[y].tid];
                locations.push(a);
            }
            function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 11,
                    center: new google.maps.LatLng(40.7595911, -73.7490067),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var infowindow = new google.maps.InfoWindow();

                var marker, i,assets;
                var icon = {
                    icon:base + "/assets/images/content/home_marker.png"
                };

                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        icon: icon.icon,
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            if(locations[i][3] == 1){
                                infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+locations[i][5]+'/'+locations[i][4]+'/"><img src="'+base+'/assets/images/homes/Coming-Soon-Sign.jpg" width="100%"></a></div><div class="map_home_name"><span>'+locations[i][0]+'</span></div>');
                            }else{
                                infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+locations[i][5]+'/'+locations[i][4]+'/"><img src="'+base+'/assets/images/homes/'+locations[i][3]+'" width="100%"></a></div><div class="map_home_name"><span>'+locations[i][0]+'</span></div>');
                            }
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6kwbUaYPPDJltTBXnLwEwVgvrklMBKkg&callback=initMap">
        </script>
        <div class="serch_opacity">
        </div>
        <div class="col-md-6  nopadd center main_search">
            <div class="head_search clear">
                <form method="post" action="<?=$baseurl?>/region/headsearch/">
                    <div class="col-lg-3 col-md-3 col-sm-3  col-xs-10 mob_select_as  nopadd">
                        <div class="filter_items">
                            <select name="region">
                                <?php foreach ($params['regionss'] as $regionss) {  ?>
                                    <option value="<?=$regionss['id']?>"><?=$regionss['region']?></option>
                                <?php }  ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-10 mob_select_as nopadd">
                        <div class="filter_items">
                            <input type="number" name="minprice" placeholder="Min. Price">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3  col-xs-10 mob_select_as nopadd">
                        <div class="filter_items">
                            <input  type="number" name="maxprice" placeholder="Max. Price">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3  col-xs-10 mob_select_as nopadd">
                        <div class="filter_items">
                            <button type="submit" class="search_homes"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 nopadd clear">
        <div class="main_row clear">
            <div class="content clear">
                <div class="main_row_text clear">
                    <span>Find your new place with DLS</span>
                </div>
            </div>
        </div>
    </div>
    <div class=" col-md-12 col-lg-12 col-sm-12 col-xs-12  clear nopadd" >
        <div class="content clear">
            <div class="col-md-12 nopadd  clear" style="text-align: center">
                <div class="col-md-12 clear">
                    <p class="main_r_title">Our Services</p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-3 col-lg-3 leftNopadd">
                    <p class="main_r_pins">
                        <span><i class="fa fa-compass" aria-hidden="true"></i></span>
                    </p>
                    <p class="main_r_first">
                        <sapn>Find places anywhere in NY 5 Regions</sapn>
                    </p>
                    <p class="main_r_second">
<!--                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>-->
                    </p>
                </div>
                <div class="col-xs-12 col-md-3 col-sm-3 col-lg-3">
                    <p class="main_r_pins">
                        <span><i class="fa fa-users" aria-hidden="true"></i></span>
                    </p>
                    <p class="main_r_first">
                        <sapn>We have agents with experience</sapn>
                    </p>
                    <p class="main_r_second">
<!--                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>-->
                    </p>
                </div>
                <div class=" col-xs-12 col-md-3 col-sm-3 col-lg-3">
                    <p class="main_r_pins">
                        <span><i class="fa fa-home" aria-hidden="true"></i></span>
                    </p>
                    <p class="main_r_first">
                        <sapn>Buy beautiful properties</sapn>
                    </p>
                    <p class="main_r_second">
<!--                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>-->
                    </p>
                </div>
                <div class=" col-xs-12 col-md-3 col-sm-3 col-lg-3 rightNopadd">
                    <p class="main_r_pins">
                        <span><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                    </p>
                    <p class="main_r_first">
                        <sapn>With agent account you can list properties</sapn>
                    </p>
                    <p class="main_r_second">
<!--                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>-->
                    </p>
                </div>
            </div>
<!--            <div class="col-md-12 nopadd clear">-->
<!--                <div>-->
<!--                    <p class="main_r_title">Recently Listed Properties</p>-->
<!--                </div>-->
<!--                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd">-->
<!--                    <div class="clear">-->
<!--                        --><?php //foreach ($params['homess'] as $val) {  ?>
<!--                            <a href="region/--><?//=$val['tid']?><!--/--><?//=$val['id']?><!--/">-->
<!--                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">-->
<!--                                    <div class="res_homes">-->
<!--                                        <div class="m_res_img">-->
<!--                                            --><?php //  if($val['pic'] == 1 ){  ?>
<!--                                                <img src="--><?//=$baseurl?><!--/assets/images/homes/Coming-Soon-Sign.jpg" alt="Coming-Soon-Sign" width="100%">-->
<!--                                            --><?php // }else {  ?>
<!--                                                <img src="--><?//=$baseurl?><!--/assets/images/homes/--><?//=$val['pic']?><!--" alt="" width="100%">-->
<!--                                            --><?php //  }  ?>
<!--                                        </div>-->
<!--                                        <div class="res_texts">-->
<!--                                            <p class="res_name"><span>--><?//=$val['adress']?><!--</span></p>-->
<!--                                            <p class="res_price"><span>$ --><?//=$val['price']?><!--</span></p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        --><?php //}  ?>
<!--                    </div>-->
<!--                    <p class="oru_list_view">-->
<!--                        <span data-toggle="modal" data-target="#myModals">-->
<!--                            View on Map-->
<!--                        </span>-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div>-->




            <div class="col-md-12 nopadd clear">
                <div>
                    <p class="main_r_title">Sold Properties</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd">
                    <div class="clear">
                        <?php foreach ($params['sold'] as $val) {  ?>
                            <a href="region/<?=$val['tid']?>/<?=$val['id']?>/">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="res_homes">
                                        <div class="m_res_img">
                                            <?php   if($val['pic'] == 1 ){  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/Coming-Soon-Sign.jpg" alt="Coming-Soon-Sign" width="100%">
                                            <?php  }else {  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/<?=$val['pic']?>" alt="" width="100%">
                                            <?php   }  ?>
                                        </div>
                                        <div class="res_texts">
                                            <p class="res_name"><span><?=$val['adress']?></span></p>
                                            <p class="res_price"><span>$ <?=$val['price']?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }  ?>
                    </div>
                </div>
            </div>







            <div class="col-md-12 nopadd clear">
                <div>
                    <p class="main_r_title">Contract to buy</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd">
                    <div class="clear">
                        <?php foreach ($params['buy'] as $val) {  ?>
                            <a href="region/<?=$val['tid']?>/<?=$val['id']?>/">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="res_homes">
                                        <div class="m_res_img">
                                            <?php   if($val['pic'] == 1 ){  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/Coming-Soon-Sign.jpg" alt="Coming-Soon-Sign" width="100%">
                                            <?php  }else {  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/<?=$val['pic']?>" alt="" width="100%">
                                            <?php   }  ?>
                                        </div>
                                        <div class="res_texts">
                                            <p class="res_name"><span><?=$val['adress']?></span></p>
                                            <p class="res_price"><span>$ <?=$val['price']?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }  ?>
                    </div>
                </div>
            </div>










            <div class="col-md-12 nopadd clear">
                <div>
                    <p class="main_r_title">Contract to Sell</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd">
                    <div class="clear">
                        <?php foreach ($params['sell'] as $val) {  ?>
                            <a href="region/<?=$val['tid']?>/<?=$val['id']?>/">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="res_homes">
                                        <div class="m_res_img">
                                            <?php   if($val['pic'] == 1 ){  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/Coming-Soon-Sign.jpg" alt="Coming-Soon-Sign" width="100%">
                                            <?php  }else {  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/<?=$val['pic']?>" alt="" width="100%">
                                            <?php   }  ?>
                                        </div>
                                        <div class="res_texts">
                                            <p class="res_name"><span><?=$val['adress']?></span></p>
                                            <p class="res_price"><span>$ <?=$val['price']?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }  ?>
                    </div>
                </div>
            </div>




            <div class="col-md-12 nopadd clear">
                <div>
                    <p class="main_r_title">Own Properties</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd">
                    <div class="clear">
                        <?php foreach ($params['own'] as $val) {  ?>
                            <a href="region/<?=$val['tid']?>/<?=$val['id']?>/">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="res_homes">
                                        <div class="m_res_img">
                                            <?php   if($val['pic'] == 1 ){  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/Coming-Soon-Sign.jpg" alt="Coming-Soon-Sign" width="100%">
                                            <?php  }else {  ?>
                                                <img src="<?=$baseurl?>/assets/images/homes/<?=$val['pic']?>" alt="" width="100%">
                                            <?php   }  ?>
                                        </div>
                                        <div class="res_texts">
                                            <p class="res_name"><span><?=$val['adress']?></span></p>
                                            <p class="res_price"><span>$ <?=$val['price']?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }  ?>
                    </div>
                </div>
            </div>




            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd clear fancVideosMain">
                <div>
                    <p class="main_r_title">Videos</p>
                </div>
                <div class="fancVideos left rightNopadd">
                    <p class="vid_title">Loan Process</p>
                    <video width="100%" height="auto" controls="" poster="http://www.dlshomesonline.com/assets/images/VideoScreens/Loan Process.png">
                        <source src="http://www.dlshomesonline.com/assets/videos/Loan Process..mp4" type="video/mp4">
                    </video>
                </div>
                <div class="fancVideos left">
                    <p class="vid_title">Loan Approved</p>
                    <video width="100%" height="auto" controls="" poster="http://www.dlshomesonline.com/assets/images/VideoScreens/Loan Approved.png">
                        <source src="http://www.dlshomesonline.com/assets/videos/Loan Approved.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="fancVideos left">
                    <p class="vid_title">Clear to Close</p>
                    <video width="100%" height="auto" controls="" poster="http://www.dlshomesonline.com/assets/images/VideoScreens/Clear to Close.png">
                        <source src="http://www.dlshomesonline.com/assets/videos/Clear to Close.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="fancVideos left">
                    <p class="vid_title">Refinance</p>
                    <video width="100%" height="auto" controls="" poster="http://www.dlshomesonline.com/assets/images/VideoScreens/Refinance.png">
                        <source src="http://www.dlshomesonline.com/assets/videos/Refinance.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="fancVideos left">
                    <p class="vid_title">Thank You!</p>
                    <video width="100%" height="auto" controls="" poster="http://www.dlshomesonline.com/assets/images/VideoScreens/Thank You!.png">
                        <source src="http://www.dlshomesonline.com/assets/videos/Thank You!.mp4" type="video/mp4">
                    </video>
                </div>
            </div>
            <div class="col-md-12 nopadd clear loan_pics_main">
                <div>
                    <p class="main_r_title"></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a class="grouped_elements" rel="group1" href="<?=$baseurl?>/assets/images/g_pdfs/1 HOUSE WISH LIST-1.png">
                        <img class="my_pdf_to_img" src="<?=$baseurl?>/assets/images/g_pdfs/1 HOUSE WISH LIST-1.png" alt="HOUSE WISH LIST" width="100%">
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                    <a class="grouped_elements" rel="group1" href="<?=$baseurl?>/assets/images/g_pdfs/2 PAPERWORK NEEDED FOR LOAN-1.png">
                        <img class="my_pdf_to_img" src="http://www.dlshomesonline.com/assets/images/g_pdfs/2 PAPERWORK NEEDED FOR LOAN-1.png" alt="HOUSE WISH LIST" width="100%">
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                    <a class="grouped_elements" rel="group1" href="<?=$baseurl?>/assets/images/g_pdfs/3 FHA vs CONV-1.png">
                        <img class="my_pdf_to_img" src="<?=$baseurl?>/assets/images/g_pdfs/3 FHA vs CONV-1.png" alt="HOUSE WISH LIST" width="100%">
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <a class="grouped_elements" rel="group1" href="<?=$baseurl?>/assets/images/g_pdfs/4 What PITI STANDS FOR-1.png">
                        <img class="my_pdf_to_img" src="<?=$baseurl?>/assets/images/g_pdfs/4 What PITI STANDS FOR-1.png" alt="HOUSE WISH LIST" width="100%">
                    </a>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd clear">
                <div>
                    <p class="main_r_title"></p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd clear">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 left">
                        <a href="https://www.mediacenternow.com/temp/20475_2.pdf?gd=08a84407-9570-4935-8889-29eb004ba55d" target="_blank">
                            <p class="main_links">
                                Click Here for the 30 Year FHLMC Rates On 30-Year Fixed-Rate Mortgage Chart
                            </p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd clear">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 center">
                        <a href="https://www.mediacenternow.com/temp/20475_1.pdf?gd=8d114026-c742-49a6-9d94-fb92d1172bf0" target="_blank">
                            <p class="main_links">
                                Click Here for the 200 Year Historical Rates On 30-Year Fixed-Rate Mortgage Chart
                            </p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadd clear">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 right" style="margin-bottom: 43px;">
                        <a href="https://www.mediacenternow.com/temp/20475_7.pdf?gd=6e57cb4b-b0f0-43a1-b54b-f8bdf8e53411" target="_blank">
                            <p class="main_links">
                                Click Here for the Housing Prices Chart
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $ds = DIRECTORY_SEPARATOR;
    $base_dir = realpath(dirname(__FILE__)  . $ds . '..') . $ds;
    require_once("{$base_dir}Pages{$ds}mainourmap.php");
?>