<?php
//    echo '<pre>';
//    var_dump($params['result']);
//    echo '</pre>';

?>
<div class="col-md-12 nopadd clear">
    <div>
        <p class="main_r_title">My Favourite Listings</p>
    </div>
    <div class="col-md-12 nopadd clear">
        <?php foreach ($params['result'] as $val) {  ?>
            <a href="<?=$baseurl?>/region/<?=$val['homes']['tid']?>/<?=$val['homes']['id']?>/">
                <div class="col-md-4">
                    <div class="res_homes">
                        <div class="m_res_img">
                            <img src="<?=$baseurl?>/assets/images/homes/<?=$val['homes']['pic']?>" alt="" width="100%">
                        </div>
                        <div class="res_texts">
                            <p class="res_name"><span><?=$val['homes']['adress']?></span></p>
                            <p class="res_price"><span>$ <?=$val['homes']['price']?></span></p>
                        </div>
                    </div>
                    <p class="res_delete">
                        <span data-code="<?=$val['homes']['id']?>" class="__del_fav"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
                    </p>
                </div>
            </a>
        <?php }  ?>
    </div>
</div>