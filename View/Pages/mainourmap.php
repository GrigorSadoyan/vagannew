<div id="myModals" class="modal fade " role="dialog">
    <p class="our_h_title">Our Homes</p>
    <div id="ourmap" class="my_map_mod"></div>
</div>

<script type="text/javascript">
    var obj = <?= json_encode($params['homess']); ?>;
    var y;
    var Mylocations = [];
    for(y = 0; y <obj.length; y++ ){
        var a =[obj[y].adress, obj[y].lat, obj[y].long,obj[y].pic,obj[y].id, obj[y].tid];
        Mylocations.push(a);
    }
    function initMaps() {
        var gmap = new google.maps.Map(document.getElementById('ourmap'), {
            zoom: 10,
            center: new google.maps.LatLng(40.81688, -73.731033),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow();

        var marker, i,assets;
        var icon = {
            icon:base + "/assets/images/content/home_marker.png"
        };
        console.log(Mylocations);
        for (i = 0; i < Mylocations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Mylocations[i][1], Mylocations[i][2]),
                icon: icon.icon,
                map: gmap
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    if(Mylocations[i][3] == 1){
                        infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+Mylocations[i][5]+'/'+Mylocations[i][4]+'/"><img src="'+base+'/assets/images/homes/Coming-Soon-Sign.jpg" width="100%"></a></div><div class="map_home_name"><span>'+Mylocations[i][0]+'</span></div>');
                    }else{
                        infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+Mylocations[i][5]+'/'+Mylocations[i][4]+'/"><img src="'+base+'/assets/images/homes/'+Mylocations[i][3]+'" width="100%"></a></div><div class="map_home_name"><span>'+Mylocations[i][0]+'</span></div>');
                    }
                    infowindow.open(gmap, marker);
                }
            })(marker, i));
        }
    }
    initMaps();
</script>