<div class="col-md-12 terms_main">
    <div class="col-md-7 center">
        <p class="dls_terms_title">
            DlsHomesOnline.com's Privacy Policy &
            Terms of Service
        </p>

        <div class="terms_types">
            <p class="term_type_title">
                Terms & Conditions
            </p>
            <p class="term_type_text">
                Please read the following information carefully. The information contained on this
                website is communicated and issued by DLS Homes Online LLC (“DLS”) in accordance
                with the following terms and conditions. By accessing any part of this website, you will
                be deemed to have accepted the terms and conditions in this Legal Notice in full. If you
                do not accept these terms and conditions, please do not continue to access this
                website.
            </p>
            <p class="term_type_title">
                No Offer or Advice
            </p>
            <p class="term_type_text">
                The information contained on this website is not intended to and does not constitute an
                offer, solicitation, inducement, invitation or commitment to purchase, subscribe to,
                provide or sell any securities, service or product or to provide any recommendations on
                which visitors to this website should rely for financial, securities, investment or other
                advice or to take any decision based on such information. Visitors to this website are
                encouraged to seek individual advice from their legal, financial, personal and other
                advisers before making any investment or financial decision or purchasing any financial,
                securities or investment related service or product.
            </p>
            <p class="term_type_title">
                Accuracy of Information
            </p>
            <p class="term_type_text">
                Although DLS has taken reasonable care that the information contained on this website
                or delivered to you by e-mail, if any, is accurate at the time of publication, such
                information is provided “as is” for only informational purposes as of the date of
                publication, and no representation or warranty (including liability towards third parties),
                expressed or implied, is made (or accepted) as to its accuracy or completeness or
                fitness for any purpose by DLS or its affiliates. Under no circumstances will DLS or its
                affiliates be liable for any direct, indirect, incidental, special or consequential loss or
                damage caused by reliance on this information or for the risks inherent in the financial
                markets. Information regarding the past performance of an investment is not necessarily
                indicative of the future performance of that or any other investment. The value of
                investments may fall as well as rise.
            </p>
            <p class="term_type_title">
                Forward-Looking Statements
            </p>
            <p class="term_type_text">
                This website includes statements that are, or may be deemed to be, “forward looking
                statements”. These forward looking statements can be identified by the use of forward
                looking terminology, including the terms “believes”, “estimates”, “anticipates”, “expects”,
                “intends”, “may”, “will” or “should” or, in each case, their negative or other variations or
                comparable terminology. These forward looking statements include all matters that are
                not historical facts. They appear in a number of places throughout this website and
                include statements regarding the intentions, beliefs or current expectations of DLS
                concerning, among other things, the investment performance, results of operations,
                financial condition, liquidity and prospects of DLS. By their nature, forward looking
                statements involve risks and uncertainties because they relate to events and depend on
                circumstances that may or may not occur in the future. Forward looking statements are
                not guarantees of future performance. DLS’s actual investment performance, results of
                operations, financial condition and liquidity may differ materially from the impression
                created by the forward looking statements contained on this website. In addition, even if
                the investment performance, results of operations, financial condition and liquidity of
                DLS are consistent with the forward looking statements contained in this website, those
                results or developments may not be indicative of results or developments in subsequent
                periods. DLS does not undertake to update any of these forward-looking statements.
            </p>
            <p class="term_type_title">
                Limitations of Liability
            </p>
            <p class="term_type_text">
                To the maximum extent permitted by applicable law and regulatory requirements, DLS
                specifically disclaims any liability for errors, inaccuracies or omissions on this website
                and for any loss (whether direct or indirect) or damage resulting from its use, whether
                caused by negligence or otherwise. Visitors who choose to access this website do so on
                their own initiative and agree to assume responsibility for determining whether any legal
                or regulatory considerations limit their access to or use of information contained on this
                website. Visitors are responsible for compliance with all local laws and regulations.
            </p>
            <p class="term_type_title">
                Ownership
            </p>
            <p class="term_type_text">
                Unless otherwise expressly noted, all content included on this website, including,
                without limitation, images, logos, articles and other materials, are protected by
                copyrights, and/or other intellectual property owned, controlled or licensed by DLS or its
                affiliates. All trademarks and logos displayed on this website are the property of their
                respective owners, who may or may not be affiliated with DLS. Visitors are responsible
                for complying with all applicable copyright, trademark and other applicable laws. We
                allow visitors to make copies of this website as necessary incidental acts during their
                viewing of the website. Visitors can print, for their personal use, as much of the website
                as is reasonable for private purposes. All other use is strictly prohibited.
            </p>
            <p class="term_type_title">
                Linked Websites

            </p>
            <p class="term_type_text">
                Visitors cannot link our website to another website without our express permission. We
                do not permit others to link to or frame this website or any portion thereof. We are not
                responsible for the security, content, or privacy policies of any other website, including
                any website through which visitors may have gained access to our website, or to which
                visitors may gain access from our website. We do not accept any liability in connection
                with any such websites or links. Where we provide a link to a third party’s website, we
                do so because we believe in good faith that such website contains or may contain
                material which is relevant to that on our website. The link does not signify that we have
                reviewed or approved the contents of the third party’s website. We have no control over
                the contents of third party websites, and accept no responsibility for them or from any
                loss or damage that may arise from your use of them.
            </p>
            <p class="term_type_title">
                Selling Restrictions
            </p>
            <p class="term_type_text">
                Access to the information contained on this website may be restricted by laws and
                regulations applicable to the user. The information on this website does not constitute
                either an offer to sell or a solicitation or an offer to purchase in any jurisdiction in which
                this type of offer or solicitation is unlawful, or in which a person making such an offer or
                solicitation does not hold the necessary authorization to do so, or at all. Accordingly,
                persons accessing the information on this website are themselves responsible for
                ascertaining the legal requirements which would affect their acquisition or sale of any
                securities or investment.
            </p>
            <p class="term_type_title">
                Governing Law
            </p>
            <p class="term_type_text">
                The terms and conditions of this legal notice shall be governed by and construed in
                accordance with the laws of the State of New York. Visitors agree that any dispute or
                action at law or in equity arising out of or relating to these terms or any visitor’s use of
                this website shall be commenced only in the state or federal courts located in New York
                county, New York, and visitors hereby consent and submit to the personal jurisdiction of
                such courts for the purposes of any such dispute or action.
            </p>
            <p class="term_type_title">
                Access and Variation
            </p>
            <p class="term_type_text">
                DLS reserves the right to disable or restrict any user’s access if in its opinion such user
                has failed to comply with any of the terms of this Legal Notice. DLS also reserves the
                right to vary the terms of this Legal Notice from time to time by amending this page, and
                users should check it from time to time to note any changes made.
            </p>
            <p class="term_type_title">
                Equal Opportunity
            </p>
            <p class="term_type_text">
                DLS is an equal opportunities employer and welcomes applications from all qualified
                persons regardless of race, sex, color, age, disability, genetic information, religious
                affiliation, genetic information, marital status, sexual orientation, gender
                identity/reassignment or expression, citizenship, pregnancy or maternity, veteran status,
                national origin, age or any other basis.
            </p>
        </div>
    </div>
</div>