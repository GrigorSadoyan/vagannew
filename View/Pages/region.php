<div class="col-md-12 nopadd clear">
    <div class="col-md-6 nopadd my_map" id="map">
    </div>
    <div class="col-md-6 nopadd ">
        <div class="col-md-12 nopadd my_houses_inf my_map">
            <p class="my_houses_title"><?=$params['reg_name']['region']?></p>
            <?php  foreach ($params['homes'] as $val){ ?>
            <a href="<?=$baseurl?>/region/<?=$params['reg_name']['id']?>/<?=$val['id']?>">
                <div class="col-md-6 by_reg_mains">
                    <div class="">
                        <div class="this_h_img">
                            <?php   if($val['pic'] == 1 ){  ?>
                            <img src="<?=$baseurl?>/assets/images/homes/Coming-Soon-Sign.jpg" alt="<?=$val['adress']?>">
                            <?php  }else{ ?>
                                <img src="<?=$baseurl?>/assets/images/homes/<?=$val['pic']?>" alt="<?=$val['adress']?>">
                            <?php  } ?>
                            <div class="by_reg_view"><p><i class="fa fa-eye" aria-hidden="true"></i></p></div>
                        </div>
                        <div class="by_reg_inf">
                            <p class="by_reg_name"><?=$val['adress']?></p>
                            <div class="by_reg_props">
                                <ul>
                                    <li>
                                        <p><i class="fa fa-bed" aria-hidden="true"></i><span> <?=$val['bad']?></span></p>
                                    </li>
                                    <li class="by_reg_lefting">
                                        <p><i class="fa fa-bath" aria-hidden="true"></i><span> <?=$val['bath']?></span></p>
                                    </li>
                                    <li class="by_reg_lefting">
                                        <p><i class="fa fa-arrows-alt" aria-hidden="true"></i><span> <?=$val['sqft']?> sq/ft</span></p>
                                    </li>
                                </ul>
                            </div>
                            <p class="by_reg_price">$<?=$val['sqft']?></p>
                        </div>
                    </div>
                </div>
            </a>
            <?php   } ?>

        </div>
    </div>
</div>
<script>
    var regions = <?= json_encode($params['reg_name']); ?>;
    var obj = <?= json_encode($params['homes']); ?>;
    var y;
    var Region = [regions.lat,regions.long,regions.id];
    var locations = [];
        for(y = 0; y <obj.length; y++ ){
            var a =[obj[y].adress, obj[y].lat, obj[y].long,obj[y].pic,obj[y].id];
            locations.push(a);
        }
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: new google.maps.LatLng(Region[0], Region[1]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow();

        var marker, i,assets;
        var icon = {
            icon:base + "/assets/images/content/home_marker.png"
        };

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                icon: icon.icon,
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    if(locations[i][3] == 1){
                        infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+regions.id+'/'+locations[i][4]+'/"><img src="'+base+'/assets/images/homes/Coming-Soon-Sign.jpg" width="100%"></a></div><div class="map_home_name"><span>'+locations[i][0]+'</span></div>');
                    }else{
                        infowindow.setContent('<div class="map_imgs"><a href="'+base+'/region/'+regions.id+'/'+locations[i][4]+'/"><img src="'+base+'/assets/images/homes/'+locations[i][3]+'" width="100%"></a></div><div class="map_home_name"><span>'+locations[i][0]+'</span></div>');
                    }
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6kwbUaYPPDJltTBXnLwEwVgvrklMBKkg&callback=initMap">
</script>