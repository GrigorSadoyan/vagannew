<?php

namespace Controller;
use Core\Controller as BaseController;
use Model\Region as Regions;
use Model\Homes;
use Model\Likes;
use Model\Gallery;
class Region extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 2 && $route[0] == 'region' && is_numeric($route[1])) {
                $this->index($route[1]);
            }elseif($countRoute == 3 && $route[0] == 'region' && is_numeric($route[1]) && is_numeric($route[2])) {
                $this->FindHome($route[1],$route[2]);
            }elseif($countRoute == 2 && $route[0] == 'region' && $route[1] == 'favourite') {
                $this->Myfavourites();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0] == 'region' && $route[1] == 'headsearch'){
                $this->SearchByHead();
            }elseif($countRoute == 4 && $route[0] == 'region' && $route[1] == 'home' && $route[2] == 'like' && $route[3] == 'add') {
                $this->AddLike();
            }elseif($countRoute == 4 && $route[0] == 'region' && $route[1] == 'home' && $route[2] == 'like' && $route[3] == 'delete') {
                $this->RemoveLike();
            }
        }
    }
    private function Myfavourites(){
        $_oLikes = new Likes();
        $token = $this->userInfo['token'];
        $_aLikes = $_oLikes->findByName(array('fild_name'=>'token','fild_val'=>$token,'order'=>array('desc'=>'data')));
        $_oHomes = new Homes();
        foreach ($_aLikes as &$val){
            $_aHomes = $_oHomes->findById(intval($val['tid']));
            $val['homes'] = $_aHomes;
        }
        $this->result['result'] = $_aLikes;
        $this->renderView("Pages/favourite",'favourite',$this->result);
    }
    private function RemoveLike(){
        $tid = $_POST['id'];
        $_oLikes = new Likes();
        $_oLikes->deleteBag($tid);
        echo json_encode(array('error'=>true));
    }
    private function AddLike(){
        $_POST['token'] = $this->userInfo['token'];
        $_oLikes = new Likes();
        $_POST['tid'] = intval($_POST['id']);
        unset($_POST['id']);
        $_oLikes->_post=$_POST;
        $_oLikes->insert();

    }
    public function index($id)
    {
        $_oRegions = new Regions();
        $a_Regions = $_oRegions->findById($id);
        $this->result['reg_name'] = $a_Regions;
        $_oHomes = new Homes();
        $_aHomes = $_oHomes->findByName(array('fild_name'=>'tid','fild_val'=>intval($a_Regions['id']),'order'=>array('desc'=>'data')));
        $this->result['homes'] = $_aHomes;
        $this->renderWithoutFooter("Pages/region",'region',$this->result);
    }
    private function FindHome($regId = false,$homeId)
    {
        $_oRegions = new Regions();
        $a_Regions = $_oRegions->findById($regId);
        $this->result['reg_name'] = $a_Regions;
        $_oHomes = new Homes();
        $_aHomes = $_oHomes->findByName(array('fild_name' => 'tid', 'fild_val' => intval($a_Regions['id']), 'order' => array('desc' => 'data')));
        $this->result['homes'] = $_aHomes;

        $oGallery = new Gallery();
        $aGallery = $oGallery->findByName(array('fild_name'=>'tunid','fild_val'=>$_aHomes[0]['id'],'order'=>array('desc'=>'id')));
        $this->result['gallery'] = $aGallery;
        $_aHome = $_oHomes->findById($homeId);
        $_oLikes = new Likes();
        $tid = intval($_aHome['id']);
        if(isset($_COOKIE['user_in'])){
            if(strlen($_COOKIE['user_in']) == 40){
                $_aHome['likes'] = $_oLikes->findByMultyName(array('tid' => $tid , 'token' => $this->userInfo['token']));
            }
        }

        $this->result['this_home'] = $_aHome;
        $this->renderWithoutFooter("Pages/home", 'home', $this->result);
    }
    private function SearchByHead(){

        $region = intval($_POST['region']);
        $minprice = intval($_POST['minprice']);
        $maxprice = intval($_POST['maxprice']);

        if($maxprice == 0){
            $maxprice = 100000000;
        }

        $_oRegions = new Regions();
        $a_Regions = $_oRegions->findById($region);
        $this->result['reg_name'] = $a_Regions;
        $_oHomes = new Homes();
        $_aHomes = $_oHomes->SelectFilter($region, $minprice, $maxprice);
        $this->result['homes'] = $_aHomes;
        $this->renderWithoutFooter("Pages/searchresult",'searchresult',$this->result);
        //echo json_encode(array('results' => true));
    }
}