<?php

namespace Controller;
use Core\Controller as BaseController;
use Model\About as mAbout;
use model\Partners as Partner;
class About extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'about') {
                $this->index();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
    }

    public function index()
    {
        $mPartner = new Partner();
        $aPartner = $mPartner->findAll(array('order'=>array('desc'=>'id')));
        $this->result['partners']=$aPartner;

        $oAbout = new mAbout();
        $aAbout = $oAbout->findAll(array('order'=>array('asc'=>'ord')));
        $this->result['result']=$aAbout;
        $this->renderView("Pages/about",'about',$this->result);
    }
}