<?php
/**
 * Created by PhpStorm.
 * User: maruq
 * Date: 17.09.2017
 * Time: 12:25
 */
namespace Controller;
use Core\Controller as BaseController;
use Model\Region;
use Model\User;

class Signin extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'signin') {
                $this->index();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 1 && $route[0]=='signin'){
                $this->AddUser();
            }
        }
    }
    private function index()
    {
        $_oRegion = new Region;
        $this->result['regions'] = $_oRegion->findAll(array());
        $this->renderView("Pages/signin",'signin',$this->result);
    }
    private function AddUser(){
        $regEmail = $_POST['email'];
        $_oUser = new User();
        $_aUser = $_oUser->findByName(array('fild_name'=>'email','fild_val'=>$regEmail));
        if(count($_aUser) == 0){
            if($_POST['password'] === $_POST['repassword']){
                unset($_POST['repassword']);
                $tok = sha1($_POST['email'].$_POST['password']);
                $_POST['token'] = $tok;
                $_POST['regionid'] = intval( $_POST['regionid']);
                unset($_POST['password']);
                $_aPost=$_POST;
                $_oUser->_post=$_aPost;
                $_oUser->insert();
                setcookie("user_in", $tok,time()+72000,'/');
                header("Location:$this->baseurl/");
            }else{
                var_dump("pass_no_match");
                setcookie("pass_no_match", '1',time()+2,'/');
                header("Location:$this->baseurl/signin/");
            }
        }else{
            var_dump("try_another_mail");
            setcookie("try_another_mail", '1',time()+2,'/');
            header("Location:$this->baseurl/signin/");
        }

    }



}