<?php

namespace Controller;
use Core\Controller as BaseController;

class Forgotpass extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'forgotpass') {
                $this->index();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
    }

    public function index()
    {
        $this->renderView("Pages/forgotpass",'forgotpass',$this->result);
    }
}