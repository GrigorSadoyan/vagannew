<?php
namespace Controller;
use Core\Controller as BaseController;
use Model\Region as Regions;
use Model\Homes;

class Mains extends BaseController{

    public function __construct()
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $this->index();
        }else{
            $this->renderNotFound('main');
            die();
        }
    }

    public function index()
    {
        $this->result['result'] = '111';
        $_oRegions = new Regions();
        $a_Regions = $_oRegions->findAll(array());
        $this->result['regionss'] = $a_Regions;
        $_oHomes = new Homes();
       // $_aHomes = $_oHomes->findByName(array('fild_name'=>'hot_sale','fild_val'=>'1'));
        $_aHomesall = $_oHomes->findAll(array());
       // $this->result['homess'] = $_aHomes;
        $this->result['homeall'] = $_aHomesall;

        $Own = $_oHomes->findByName(array('fild_name'=>'type_id','fild_val'=>'1'));
        $this->result['own'] = $Own;

        $Sell = $_oHomes->findByName(array('fild_name'=>'type_id','fild_val'=>'2'));
        $this->result['sell'] = $Sell;

        $Buy = $_oHomes->findByName(array('fild_name'=>'type_id','fild_val'=>'3'));
        $this->result['buy'] = $Buy;

        $Sold = $_oHomes->findByName(array('fild_name'=>'type_id','fild_val'=>'4'));
        $this->result['sold'] = $Sold;

        $this->renderViewForMainPage("Pages/main","main",$this->result);
    }
}
