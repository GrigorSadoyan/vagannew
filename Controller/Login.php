<?php

namespace Controller;
use Core\Controller as BaseController;
use Model\User;

class Login extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'login') {
                $this->index();
            }elseif($countRoute == 2 && $route[0] == 'login' && $route[1] == 'changepassword'){
                $this->ChangePassword();
            }elseif($countRoute == 2 && $route[0] == 'login' && $route[1] == 'forgotpassword'){
                $this->ForgotPassword();
            }elseif($countRoute == 2 && $route[0] == 'login' && $route[1] == 'logout'){
                $this->Logout();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if($countRoute == 2 && $route[0]=='login' && $route[1] == 'log'){
                $this->Login();
            }elseif($countRoute == 2 && $route[0] == 'login' && $route[1] == 'reg'){
                $this->Registration();
            }elseif($countRoute == 2 && $route[0] == 'login' && $route[1] == 'forgotpassword'){
                $this->ForgotPasswordChange();
            }
        }
    }
    private function index(){
    $this->renderView("Pages/login",'login',$this->result);
    }
    private function Registration(){
        $regEmail = $_POST['email'];
        $_oUser = new User();
        $_aUser = $_oUser->findByName(array('fild_name'=>'email','fild_val'=>$regEmail));
        if(count($_aUser) == 0){
            $tok = sha1($_POST['email'].$_POST['password']);
            $_POST['token'] = $tok;
            unset($_POST['password']);
            $_aPost=$_POST;
            $_oUser->_post=$_aPost;
            $_oUser->insert();
            setcookie("user_in", $tok,time()+72000,'/');
            header("Location:$this->baseurl/");
        }else{
            var_dump("try_another_mail");die;
//            setcookie("try_another_mail", '1',time()+2,'/');
//            header("Location:$this->baseurl/signin/");
        }
    }
    private function ChangePassword(){
        $this->renderView("Pages/changepassword",'changepassword',$this->result);
    }
    private function Login(){
        $email = $_POST['email'];
        $pass = $_POST['password'];
        $tok = sha1($email.$pass);
        $_oUser = new User();
        $_aUser = $_oUser->findByMultyName(array('email'=>$email,'token'=>$tok ));
        if(count($_aUser) != 0){
            setcookie("user_in", $tok,time()+72000,'/');
            header("Location:$this->baseurl/");
        }else{
            var_dump('chka');die;
        }
    }
    private function Logout(){
        session_destroy();
        setcookie("user_in", NULL, time()-3600, "/");
        header("Location:$this->baseurl/");
    }
    private function ForgotPassword(){
        $this->renderView("Pages/forgotpassword",'forgotpassword',$this->result);
    }
    private function ForgotPasswordChange(){
        $_oUser = new User();
        $email = $_POST['email'];
        $_aUser = $_oUser->findByName(array('fild_name'=>'email','fild_val'=>$email));
        $id = intval($_aUser[0]['id']);
        var_dump($id);die;
        if($_aUser > 0){
            $new = $this->generateRandomString(12);
            unset($_POST);
            $_POST['email'] = $email;
            $_POST['token'] = sha1($email.$new);
            $_oUser->_put = $_POST;
            $_oUser->setId($id);
            $_oUser->update();
            $message = "Your new password for tntesakan.am is ".$new;
            mail($email, 'New Password tntesakan.am', $message);
        }else{
            var_dump('chka');die;
        }

        //$newpassword = $this->generateRandomString();
        //var_dump($newpassword);die;
        //$this->renderView("Pages/forgotpassword",'forgotpassword',$this->result);
    }
}